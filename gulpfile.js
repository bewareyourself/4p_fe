const syntax = "sass"; // Syntax: sass or scss;
const pugSyntax = "pug"; // Syntax: sass or scss;

const gulp = require("gulp"),
    gutil = require("gulp-util"),
    sass = require("gulp-sass"),
    browserSync = require("browser-sync"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    cleancss = require("gulp-clean-css"),
    cssnano = require("gulp-cssnano"),
    rename = require("gulp-rename"),
    autoprefixer = require("gulp-autoprefixer"),
    notify = require("gulp-notify"),
    rsync = require("gulp-rsync"),
    del = require("del"),
    imagemin = require("gulp-imagemin"),
    pngquant = require("imagemin-pngquant"),
    cache = require("gulp-cache");
    pug = require("gulp-pug");
    svgSprite = require("gulp-svg-sprite");
    svgstore = require("gulp-svgstore");
    svgmin = require("gulp-svgmin");
    inject = require("gulp-inject");


sass.compiler = require('node-sass');

gulp.task("browser-sync", function () {
    browserSync({
        server: {
            baseDir: "dist"
        },
        notify: false,
        // open: false,
        // online: false, // Work Offline Without Internet Connection
        // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
    })
});

gulp.task("styles", function () {
    return gulp.src("app/assets/" + syntax + "/**/*." + syntax + "")
        .pipe(sass({outputStyle: "expanded"}).on("error", notify.onError()))
        .pipe(rename({suffix: ".min", prefix: ""}))
        .pipe(autoprefixer(["last 15 versions"]))
        .pipe(cleancss({level: {1: {specialComments: 0}}})) // Opt., comment out when debugging
        .pipe(gulp.dest("dist/assets/css"))
        .pipe(browserSync.stream())
});

gulp.task("js", function () {
    return gulp.src([
        "node_modules/jquery/dist/jquery.js",
        "node_modules/bootstrap/dist/js/bootstrap.bundle.js",
        "node_modules/owl.carousel/dist/owl.carousel.min.js",
        "app/assets/vendor/elevate-zoom.js",
        "app/assets/vendor/ubislider/examples/ecommerce/js/ubislider.min.js",
        "app/assets/vendor/ubislider/examples/ecommerce/js/scripts.js",
        "node_modules/magnific-popup/dist/jquery.magnific-popup.js",
        "app/assets/js/autocomplete.js",
        "app/assets/js/common.js",
    ])
        .pipe(concat("scripts.min.js"))
        // .pipe(uglify()) // Minify js (opt.)
        .pipe(gulp.dest("dist/assets/js"))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task("rsync", function () {
    return gulp.src("app/**")
        .pipe(rsync({
            root: "app/",
            hostname: "username@yousite.com",
            destination: "yousite/public_html/",
            // include: ["*.htaccess"], // Includes files to deploy
            exclude: ["**/Thumbs.db", "**/*.DS_Store"], // Excludes files from deploy
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }))
});

gulp.task("clean", function () {
    return del.sync("dist");
});

gulp.task("clear", function () {
    return cache.clearAll();
});

gulp.task("img", function () {
    return gulp.src("app/assets/img/**/*")
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            une: [pngquant()]
        })))
        .pipe(gulp.dest("dist/assets/img"));
});

gulp.task("fonts", function () {
    return gulp.src("app/assets/fonts/**/*")
        .pipe(gulp.dest("dist/assets/fonts"));
});

gulp.task("pug", function () {
    gulp.src("app/templates/*.pug")
        .pipe(pug({pretty: "\t"}))
        .pipe(gulp.dest("dist/"))
});

gulp.task("generateSvg", function () {
    gulp.src("app/assets/svg/*.svg")
        .pipe(svgSprite({
            mode: {
                symbol: true
            }
        }))
        .pipe(gulp.dest("app/templates"))
        .pipe(browserSync.stream())
});

gulp.task("watch", ["clean", "pug", "generateSvg", "styles", "js", "fonts", "img", "browser-sync"], function () {
    gulp.watch("app/assets/" + syntax + "/**/*." + syntax + "", ["styles"]);
    gulp.watch("app/templates/**/*." + syntax + "", ["styles"]);
    gulp.watch("app/templates/**/*." + pugSyntax + "", ["pug"]);
    gulp.watch(["libs/**/*.js", "app/js/common.js"], ["js"]);
    gulp.watch("dist/*.html", browserSync.reload)
});

gulp.task("build", ["clean", "pug", "generateSvg", "styles", "js", "fonts", "img", ], function () {
    const buildCss = gulp.src([
        "app/css/main.css",
        "app/css/libs.min.css",
    ])
        .pipe(gulp.dest("dist/css"));
    const buildFonts = gulp.src("app/assets/fonts/**/*")
        .pipe(gulp.dest("dist/assets/fonts"));

    const buldJs = gulp.src("app/assets/js/**/*")
        .pipe(gulp.dest("dist/assets/js"));

    const buildHtml = gulp.src("app/*.html")
        .pipe(gulp.dest("dist"));
});

gulp.task("default", ["watch"]);
