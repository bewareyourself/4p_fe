"use strict";
/*
 *	jQuery Ubislider 2.0.0.3
 *	Demo's and documentation:
 *
 *	Copyright (c) 2017 LS Innovations LLC
 *
 *	Dual licensed under the GPL and MIT licenses.
 *	http://en.wikipedia.org/wiki/MIT_License
 *	http://en.wikipedia.org/wiki/GNU_General_Public_License
 *
 */

$.fn.ubislider = function (options) {
    return UbiSlider.init(this, options);
}
$.fn.destroyUbislider = function (options) {
    return UbiSlider.destroy(this);
}
$.fn.reloadUbislider = function (options) {
    return UbiSlider.reload(this, options);
}

var UbiSlider = {
    init: function (thisElement, options) {
        var self = this;

        $(".arrow.prev").addClass("pasive");
        var settings = $.extend({
            item: thisElement,
            arrowsToggle: false,
            type: 'standard',
            hideArrows: false,
            autoSlideOnLastClick: false,
            modalOnClick: false,
            position: 'horizontal',
            activeClass: false,
            bullets: false,
            onTopImageChange: function() {}
        }, options );

        var sliderWidth = settings.item.width();
        settings.item.addClass(settings.type);

        // Function to add active class
        var activeClass = function () {

            var widthOfSlider = settings.item.width();
            var widthOfSlide = settings.item.find('.ubislider-inner li').width();
            var activeNumber = Math.round(widthOfSlider/widthOfSlide/2) - 1;

            settings.item.find('.ubislider-inner li').eq(activeNumber).addClass('active');
        }

        // Function for adding top image for ecommerce
        var ecommerceTopImage =  function(clickedLi){
            var idValue = settings.item.attr('id');


            if(clickedLi){
                var link = clickedLi.find('img').attr('src');
                var image = clickedLi.index();
            }else{
                var link = settings.item.find('.ubislider-inner li').eq(0).find('img').attr('src');
                var image = 0;
            }

            var myImage = new Image();
            myImage.src = link;

            var searchAttr = '[data-ubislider="#'+idValue+'"]';
            $(searchAttr).attr('data-image', image);
            $(searchAttr).html(myImage);

            settings.onTopImageChange();
        };

        // Ecommerce Events
        var ecommerceEvents = function(){
            settings.item.find('.ubislider-inner li').on('click', function(){
                ecommerceTopImage($(this));
            });
        }

        // On Main image click open Modal
        var createModal = function(thisElement){
            $('body').addClass('ubi-overflow');

            var slide = thisElement.attr('data-image');


            var template = 	"<div class='ubiModal'>"+
                "<span class='ubi-close'><i class='fa fa-times' aria-hidden='true'></i></span>"+
                "<div class='ubi-arrows left'><i class='fa fa-angle-left' aria-hidden='true'></i></div>"+
                "<div class='ubi-arrows right'><i class='fa fa-angle-right' aria-hidden='true'></i></div>"+
                "<div class='ubi-main-image' data-slide='"+slide+"'>"+
                "<ul class='ubi-main-slide'>" +
                settings.item.find('.ubislider-inner').html() +
                "</ul>"+
                "</div>"+
                "<ul class='ubi-thumbs'>"+
                settings.item.find('.ubislider-inner').html();
            "</ul>"
            "</div>";

            $('body').append(template);



            var slideTime = '-'+$('.ubi-main-image').width()*slide + 'px';

            $('.ubi-main-slide').width($('.ubi-main-image').width()*($('.ubi-main-slide li').length));
            $('.ubi-main-slide').css('left',  slideTime);

            $('.ubi-main-slide li').width($('.ubi-main-image').width());
            $(".ubi-arrows.left").addClass("pasive");
        }

        // Displaying images at asigned container
        var displayImagesFromThumb = function(thisElement){
            var slide = thisElement.index();
            var slideTime = '-'+$('.ubi-main-image').width()*slide + 'px';

            $('.ubi-main-image').attr('data-slide', slide);
            $('.ubi-main-slide').width($('.ubi-main-image').width()*($('.ubi-main-slide li').length));
            $('.ubi-main-slide').css('left',  slideTime);
        }

        var moveTo = function(side){
            var slide = $('.ubi-main-image').attr('data-slide');
            var lengthOfLi = $('.ubi-main-slide li').length;

            switch(side){
                case 'left':
                    if(slide > 0){
                        slide = parseInt(slide, 10) - 1;
                        $(".ubi-arrows").removeClass("pasive");
                    }

                    if (slide === 0) {
                        $(".ubi-arrows.left").addClass("pasive");
                    }

                    var slideTime = '-'+$('.ubi-main-image').width()*slide + 'px';
                    break;
                case 'right':
                    // process
                    if (slide < lengthOfLi - 1) {
                        slide = parseInt(slide, 10) +1;
                        $(".ubi-arrows").removeClass("pasive");
                    }

                    if (slide === lengthOfLi - 1) {
                        $(".ubi-arrows.right").addClass("pasive");
                    }

                    var slideTime = '-'+$('.ubi-main-image').width()*slide + 'px';
                    break;
            }

            $('.ubi-main-image').attr('data-slide', slide);
            $('.ubi-main-slide').width($('.ubi-main-image').width()*($('.ubi-main-slide li').length));
            $('.ubi-main-slide').css('left',  slideTime);
        }

        // Function containing events
        var modalEvents = function(){
            var idValue = settings.item.attr('id');
            $('div[data-ubislider="#'+idValue+'"]').bind('click', function(){
                createModal($(this));
                eventsAfterCreateModal();
            });
        };

        var eventsAfterCreateModal = function(){
            $('.ubiModal').bind('click', function(){
                $('.ubiModal').remove();
                $('body').removeClass('ubi-overflow');
            });

            $('.ubi-close').bind('click', function(){
                $('.ubiModal').remove();
                $('body').removeClass('ubi-overflow');
            });

            $('.ubiModal .ubi-main-image img').bind('click', function(event){
                event.stopPropagation();
            });


            $('.ubi-arrows.left').bind('click', function(event){
                event.stopPropagation();
                moveTo('left');
            });

            $('.ubi-arrows.right').bind('click', function(event){
                event.stopPropagation();
                moveTo('right');
            });


            $('.ubi-thumbs li').bind('click', function(event){
                event.stopPropagation();
                displayImagesFromThumb($(this));
                $(".ubi-arrows").removeClass("pasive");
            });
        }

        // Function  to give position to aside a height and for other cases width
        var positioning = function(value){
            if(value == 'vertical'){
                var heightOfSlider = settings.item.find('.ubislider-inner li').outerWidth(true) * settings.item.find('li').length;
                settings.item.attr('data-slideTime', 0);
                settings.item.find('.ubislider-inner').width('auto');
                settings.item.find('.ubislider-inner').height(heightOfSlider);
            }else{
                var widthOfSlider = settings.item.find('.ubislider-inner li').outerWidth(true) * settings.item.find('li').length;
                settings.item.attr('data-slideTime', 0);
                settings.item.find('.ubislider-inner').width(widthOfSlider);
            }
        }

        // Function to hide arrows
        var removingArrows = function(){
            settings.item.find('.arrow').hide();
        }

        // Function creating bullets
        var createBullets = function(){
            var items = settings.item.find('.ubislider-inner li').length;
            var html = "<ul class='ubislider-bullets'>";

            for (var i = 1; i <= items; i++) {
                if(i == 1){
                    var template = "<li class='active'></li>";
                }else{
                    var template = "<li></li>";
                }
                html += template;
            }

            html += '</ul>'
            settings.item.append(html);
        }

        // Events for sliding
        var eventsBullet = function(){
            settings.item.find('.ubislider-bullets li').bind('click', function(){
                $(".ubislider .arrow").removeClass("pasive");
                slidingToBulletImage($(this));
            })
        }

        var slidingToBulletImage = function(thisElement){
            thisElement.siblings().removeClass('active');
            thisElement.addClass('active');

            var slide = thisElement.index();
            var maxNumber = thisElement.closest('.ubislider').find('.ubislider-inner li').length - 1;

            if (slide == 0) {
                $(".ubislider .arrow.prev").addClass("pasive");
            } else if (slide == maxNumber) {
                $(".ubislider .arrow.next").addClass("pasive");
            }

            var slideTime = '-'+thisElement.closest('.ubislider').find('.ubislider-inner li').outerWidth(true)*slide + 'px';

            thisElement.closest('.ubislider').attr('data-slidetime', slide);
            thisElement.closest('.ubislider').find('.ubislider-inner').css('left',  slideTime);
        }

        // Checking type of Slider
        switch(settings.type) {
            case 'standard':
                settings.item.find('li').width(sliderWidth);
                break;
            case 'thumb':
                // process


                break;
            case 'ecommerce':
                ecommerceTopImage();
                ecommerceEvents();
                break;
        }

        // Checking type of Slider
        switch(settings.modalOnClick) {
            case true:
                modalEvents();
            case false:
                break;
        }

        // Checking type of Slider
        switch(settings.option) {
            case 'bottom':
                modalEvents();

                break;
            case 'aside':

                break;
        }

        // Checking position of Slider
        switch(settings.position) {
            case 'vertical':
                positioning(settings.position);
                break;
            default:
                positioning(settings.position);

                break;
        }

        // Giving Active class to centered element
        switch(settings.activeClass) {
            case true:
                activeClass();
                break;
            default:
                break;
        }

        // Hiding Arrows
        switch(settings.hideArrows) {
            case true:
                removingArrows();
                break;
            default:
                break;
        }

        switch(settings.bullets){
            case true:
                createBullets();
                eventsBullet();
                break;
            default:
                break;
        }

        self.events(settings.item, settings, settings.position);
    },
    events: function (thisElement, settings, position) {
        var self = this;

        thisElement.find('.arrow').bind('click', function(){
            self.slider_arrow_function($(this), thisElement, settings);
        });

        // Sliding left or right if autoSlideOnLastClick is true
        if(settings.autoSlideOnLastClick){
            thisElement.find('.ubislider-inner li').bind('click', function(){
                // Switching positions to slide
                switch(position) {
                    case 'horizontal':
                        self.slideToLeftOrRight($(this), settings);
                        break;
                    case 'vertical':
                        self.slideToTopOrBottom($(this), settings);
                        break;
                    default:
                        self.slideToLeftOrRight($(this), settings);
                        break;
                }

            });
        }

    },
    slideToLeftOrRight: function(clickedElement, settings){
        // Checking if its last element on right or left
        var slideTime = parseInt(settings.item.attr('data-slideTime'), 10);
        var distanceFromLeft = settings.item.find('li').outerWidth(true) * slideTime;
        var visibleAreaCount = Math.round(settings.item.outerWidth(true) / settings.item.find('li').outerWidth(true));
        var lastChildIndex = settings.item.find('li').length - 1;
        var maxSlideTime = settings.item.find('li').length - visibleAreaCount;
        var lastVisibleIndex = Math.round((distanceFromLeft + settings.item.outerWidth(true)) / settings.item.find('li').outerWidth(true)) - 1;
        var firstVisibleIndex = Math.round((distanceFromLeft + settings.item.outerWidth(true)) / settings.item.find('li').outerWidth(true)) - visibleAreaCount;

        if(distanceFromLeft == 0 && clickedElement.closest('li').index() == 0){
            // If its first Child so sliding to the end

            settings.item.attr('data-slideTime', maxSlideTime);
            settings.item.find('.ubislider-inner').css('left', '-' + ((settings.item.find('li').outerWidth(true)) * maxSlideTime)+'px');
        }else if(distanceFromLeft == maxSlideTime*settings.item.find('li').outerWidth(true) && clickedElement.closest('li').index() == lastChildIndex){
            // If its last Child in the end of list sliding to first

            settings.item.attr('data-slideTime', '0');
            settings.item.find('.ubislider-inner').css('left', '0px');
        }else if(clickedElement.closest('li').index() == lastVisibleIndex){
            // If its the last element in visible area clicked sliding to right for one element
            slideTime++;
            settings.item.attr('data-slideTime', slideTime);
            settings.item.find('.ubislider-inner').css('left', '-' + ((settings.item.find('li').outerWidth(true)) * slideTime)+'px');
        } else if(clickedElement.closest('li').index() ==  firstVisibleIndex){
            slideTime--;
            settings.item.attr('data-slideTime', slideTime);
            settings.item.find('.ubislider-inner').css('left', '-' + ((settings.item.find('li').outerWidth(true)) * slideTime)+'px');
        }

    },
    slideToTopOrBottom: function(clickedElement, settings){
        // Checking if its last element on right or left
        var slideTime = parseInt(settings.item.attr('data-slideTime'), 10);
        var distanceFromTop = settings.item.find('li').outerHeight(true) * slideTime;
        var visibleAreaCount = Math.round(settings.item.outerHeight(true) / settings.item.find('li').outerHeight(true));
        var lastChildIndex = settings.item.find('li').length - 1;
        var maxSlideTime = settings.item.find('li').length - visibleAreaCount;
        var lastVisibleIndex = Math.round((distanceFromTop + settings.item.outerHeight(true)) / settings.item.find('li').outerHeight(true)) - 1;
        var firstVisibleIndex = Math.round((distanceFromTop + settings.item.outerHeight(true)) / settings.item.find('li').outerHeight(true)) - visibleAreaCount;


        if(distanceFromTop == 0 && clickedElement.closest('li').index() == 0){
            // If its first Child so sliding to the end

            settings.item.attr('data-slideTime', maxSlideTime);
            settings.item.find('.ubislider-inner').css('top', '-' + ((settings.item.find('li').outerHeight(true)) * maxSlideTime)+'px');
        }else if(distanceFromTop == maxSlideTime*settings.item.find('li').outerHeight(true) && clickedElement.closest('li').index() == lastChildIndex){
            // If its last Child in the end of list sliding to first

            settings.item.attr('data-slideTime', '0');
            settings.item.find('.ubislider-inner').css('top', '0px');
        }else if(clickedElement.closest('li').index() == lastVisibleIndex){
            // If its the last element in visible area clicked sliding to right for one element
            slideTime++;
            settings.item.attr('data-slideTime', slideTime);
            settings.item.find('.ubislider-inner').css('top', '-' + ((settings.item.find('li').outerHeight(true)) * slideTime)+'px');
        } else if(clickedElement.closest('li').index() ==  firstVisibleIndex){
            slideTime--;
            settings.item.attr('data-slideTime', slideTime);
            settings.item.find('.ubislider-inner').css('top', '-' + ((settings.item.find('li').outerHeight(true)) * slideTime)+'px');
        }

    },
    slider_arrow_function: function (arrowElement, thisElement, settings) {
        var self = this;

        var addingActiveClassToBullet = function(prev){
            var slideTime = parseInt(thisElement.attr('data-slideTime'), 10);

            settings.item.find('.ubislider-bullets li').removeClass('active');
            settings.item.find('.ubislider-bullets li').eq(slideTime).addClass('active');
        }



        if(arrowElement.hasClass('prev')){
            self.slider_prev(thisElement, settings);
        }else{
            self.slider_next(thisElement, settings);
        }

        switch(settings.bullets){
            case true:
                addingActiveClassToBullet();
                break;
            default:
                break;
        }

    },
    slider_prev: function (thisElement, settings) {
        var self = this;
        var slideTime = parseInt(thisElement.attr('data-slideTime'), 10);

        if(slideTime != 0){
            thisElement.find('.ubislider-inner li').each(function (key, value) {
                if($(this).hasClass('active')){
                    $(this).prev().addClass('active');
                    $(this).removeClass('active');
                    return false;
                }
            });
            slideTime--;
            $(".ubislider .arrow.next").removeClass("pasive");
            thisElement.attr('data-slideTime', slideTime);
            thisElement.find('.ubislider-inner').css('left', '-' + ((thisElement.find('.ubislider-inner li').outerWidth(true)) * slideTime)+'px');
        }

        if (slideTime == 0) {
            $(".ubislider .arrow.prev").addClass("pasive");
        }

    },
    slider_next: function (thisElement, settings) {

        var self = this;

        var widthOfSlider = thisElement.outerWidth(true);
        var widthOfSlide = thisElement.find('.ubislider-inner li').outerWidth(true);
        var num = widthOfSlider/widthOfSlide;
        var halfedSlided = false;
        var maxNumber = thisElement.find('.ubislider-inner li').length - Math.round(num);

        var slideTime = parseInt(thisElement.attr('data-slideTime'), 10);
        if(slideTime < maxNumber){
            thisElement.find('li').each(function (key, value) {
                if($(this).hasClass('active')){
                    $(this).next().addClass('active');
                    $(this).removeClass('active');
                    return false;
                }
            });
            slideTime++;
            thisElement.attr('data-slideTime', slideTime);
            $(".ubislider .arrow.prev").removeClass("pasive");
            if(halfedSlided && maxNumber - slideTime == 0){

                var widthOfCount = thisElement.find('.ubislider-inner li').outerWidth(true)*halfedSlided;
                var widthToPush = (widthOfCount-widthOfSlider) + (thisElement.find('.ubislider-inner li').outerWidth(true)*(slideTime -1));
            } else{
                var widthToPush = thisElement.find('.ubislider-inner li').outerWidth(true)*slideTime;
            }
            thisElement.find('.ubislider-inner').css('left', '-'+widthToPush+'px');
        }

        if (slideTime == maxNumber) {
            $(".ubislider .arrow.next").addClass("pasive");
        }
    },
    destroy: function(thisElement){
        var self = this;

        $('[data-ubislider="#'+thisElement.attr('id')+'"]').unbind('click');
        thisElement.find('.arrow').unbind('click');

        thisElement.find('li').unbind('click');

        // Removing widht height
        thisElement.find('.ubislider-inner').removeAttr('style');
        thisElement.find('li').removeAttr('style');
        thisElement.removeAttr('data-slideTime');

    },
    reload: function(thisElement, options){
        var self = this;

        self.destroy(thisElement);
        self.init(thisElement, options);
    }
}