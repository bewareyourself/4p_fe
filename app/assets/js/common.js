$('select').each(function () {
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    if ($this.attr('id') === 'nav-switcher') {
        $this.wrap('<div class="select nav-switcher"></div>');
    } else {
        $this.wrap('<div class="select"></div>');
    }

    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function (e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function () {
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function (e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});


$(() => {
    const header = $(".top-navigation");

    if ($(window).width() > 767) {
        $(window).scroll(() => {
            const scroll = $(window).scrollTop();

            if (scroll >= 200) {
                header.removeClass('dark-nav').addClass("clear-nav");
            } else {
                header.removeClass("clear-nav").addClass('dark-nav');
            }
        });
    } else {
        header.removeClass('dark-nav').addClass("clear-nav");
    }

});

// Navigation
const sectionOne = $("#sectionOne");
const sectionTwo = $("#sectionTwo");
const sectionThree = $("#sectionThree");

const buttonOne = $("#buttonOne");
const buttonTwo = $("#buttonTwo");
const buttonThree = $("#buttonThree");

buttonOne.on( "click", function(e) {
    e.preventDefault();
    $([document.documentElement, document.body]).animate({
        scrollTop: sectionOne.offset().top - 100
    }, 2000);
});

buttonTwo.on( "click", function(e) {
    e.preventDefault();
    $([document.documentElement, document.body]).animate({
        scrollTop: sectionTwo.offset().top - 100
    }, 2000);
});

buttonThree.on( "click", function(e) {
    e.preventDefault();
    $([document.documentElement, document.body]).animate({
        scrollTop: sectionThree.offset().top - 100
    }, 2000);
});

$(() => {

    $('#top-navbar-list').children('li.bottom-navigation__menu-list-item').each(function () {
        let $this = $(this);

        if ($this.find('div.bottom-navigation__menu-dropdown-container').length !== 0) {
            $this.addClass('has-dropdown');
        }
    });

    $('#bottom-navbar-list').children('li.dropdown-container__list-item').each(function () {
        let $this = $(this);

        if ($this.find('ul.dropdown-container__inner-list').length !== 0) {
            $this.addClass('has-dropdown');
        }
    });

    $('#navbar-list').children('li.navbar__inner-list-item').each(function () {
        let $this = $(this);

        if ($this.find('ul.navbar__inner-list').length !== 0) {
            $this.addClass('has-dropdown');
        }

        if ($this.hasClass('has-dropdown')) {
            $this.children('ul.navbar__inner-list').children('li.navbar__inner-list-item').each(function () {
                let $this = $(this);
                if ($this.find('ul.navbar__inner-list').length !== 0) {
                    $this.addClass('has-dropdown inner');
                }
            });
        }
    });
});

$(() => {
    $('.single-product_tabgroup > div').hide()
    $('.single-product_tabgroup > div:first-of-type').show();
    $('.tabs_navigation a').click(function (e) {
        e.preventDefault();
        var $this = $(this),
            tabgroup = '#' + $this.parents('.tabs_navigation').data('tabgroup'),
            others = $this.closest('li').siblings().children('a'),
            target = $this.attr('href');
        others.removeClass('active');
        $this.addClass('active');
        $(tabgroup).children('div').fadeOut(300);
        $(target).fadeIn(300);
    })

});

$(document).ready(function () {
    $(".inner_carousel")
        .owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            navText: ["<div class='navigation-button'><svg class='icon'> <use xlink:href='#nav-arrow'></use></svg></div>", "<div class='navigation-button'><svg class='icon'> <use xlink:href='#nav-arrow'></use></svg></div>"],
            responsiveClass: true,
            responsive: {
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });

    const catalogMenu = $('#catalogMenu'),
        catalogLink = $('.catalog-link'),
        otherMenu = $('#otherMenu'),
        otherLink = $('.other-link');


    otherLink.click(function () {
        $('body').toggleClass('overflow');
        catalogLink.removeClass('active');
        catalogMenu.removeClass('active');
        otherLink.toggleClass('active');
        otherMenu.toggleClass('active');
        return false;
    });

    catalogLink.click(function () {
        otherLink.removeClass('active');
        otherMenu.removeClass('active');
        catalogLink.toggleClass('active');
        catalogMenu.toggleClass('active');
        return false;
    });

    const $menu = $('.rating_block-ratings-select');

    $(document).mouseup(function (e) {
        if (!$menu.is(e.target)
            && $menu.has(e.target).length === 0) {
            $menu.removeClass('is-active');
        }
    });

    $('.toggle-ratings').on('click', () => {
        $menu.toggleClass('is-active');
    });

    $('.video-block_slider-carousel').owlCarousel({
        margin: 30,
        responsiveClass: true,
        nav: true,
        navText: ["<div class='navigation-button'><svg class='icon'> <use xlink:href='#nav-arrow'></use></svg></div>", "<div class='navigation-button'><svg class='icon'> <use xlink:href='#nav-arrow'></use></svg></div>"],
        loop: true,
        autoWidth:true,
        responsive: {
            0: {
                items: 1
            },
            568: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        }
    });

    $('.popup-youtube, .popup-text').magnificPopup({
        disableOn: 320,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: true
    });

    $('.product-carousel').owlCarousel({
        margin: 19,
        responsiveClass: true,
        nav: true,
        navText: ["<div class='navigation-button'><svg class='icon'> <use xlink:href='#nav-arrow'></use></svg></div>", "<div class='navigation-button'><svg class='icon'> <use xlink:href='#nav-arrow'></use></svg></div>"],
        loop: true,
        autoWidth:true,
        responsive: {
            0: {
                items: 1
            },
            568: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    $(".open").on("click", function(){
        $(".popup-overlay, .popup-content").addClass("active");
    });

    $(".close").on("click", function(){
        $(".popup-overlay, .popup-content").removeClass("active");
    });

    $("span.input input").on('keyup', function() {
        const value = $(this)[0].value;
        const trimmed = value.trim();
        let form = $(this).closest("form");
        let formValues = form[0].elements;
        let submitButton = $(".submit");

        if (!value) {
            $(this).attr('data-state', '');
            return
        }

        if (trimmed) {
            $(this).attr("data-state","not-empty")
        } else {
            $(this).attr("data-state","empty")
        }

        $.each(formValues, function (value, index, fak) {
            if (index.value.length > 0) {
                submitButton.removeClass("gray");
                submitButton.addClass("green");
            } else if (index.value.length === 0) {
                submitButton.removeClass("green");
                submitButton.addClass("gray");
            }
        });


    }).trigger('input');

    $("span.input textarea").on('input', function() {
        const value = $(this)[0].value;

        if (!value) {
            $(this).attr('data-state', '');
            return
        }

        const trimmed = value.trim();

        if (trimmed) {
            $(this).attr("data-state","not-empty")
        } else {
            $(this).attr("data-state","empty")
        }

    }).trigger('textarea');

    $('.main-slider').carousel();

    $('.button-dialog-form').magnificPopup({
        type: 'inline',
        preloader: false,
        showCloseBtn: false
    });

    $('.button-dialog-message').magnificPopup({
        type: 'inline',
        preloader: false,
        showCloseBtn: false
    });

    $(".popup-modal-dismiss").on('click',  function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('.profile_container img').click(function(){
        $(this).next('.select-options').toggle();
    });

    $(document).click(function(e) {
        var target = e.target;
        if (!$(target).is('.profile_container img') && !$(target).parents().is('.profile_container img')) {
            $('.select-options').hide();
        }
    });

    $("#editShipping").on('click',  function (e) {
        e.preventDefault();
        $("#editShippingForm").show();
        $("#shippingInfo").hide();
    });

    const $form = $('#mc-embedded-subscribe-form');
    if ($form.length > 0) {
        $('form input[type="submit"]').bind('click', function (event) {
            if (event) event.preventDefault();
            register($form)
        })
    }
});

$('#slider4').ubislider({
    arrowsToggle: false,
    type: 'ecommerce',
    autoSlideOnLastClick: false,
    modalOnClick: false,
    position: 'vertical',
    onTopImageChange: function () {
        $('#imageSlider4 img').elevateZoom({
            borderSize: 1,
            galleryActiveClass: "active",
            cursor: "default",
            zoomType: "window",
            zoomWindowWidth: 150,
            zoomWindowHeight: 150
        });
    }
});

$('#breadcrumbs-title').on( "click", function() {
    $('#breadcrumbs-list').slideToggle();
    $(this).toggleClass('open');
});

function register($form) {
    $('#mc-embedded-subscribe').val('Sending...');
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
        success: function (data) {
            $('#mc-embedded-subscribe').val('subscribe');
            if (data.result === 'success') {
                // Yeahhhh Success
                console.log(data.msg);
                $('#subscribe-result').css('color', 'rgb(53, 114, 210)');
                $('#subscribe-result').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>');
                $('#mce-EMAIL').val('');
            } else {
                // Something went wrong, do something to notify the user.
                console.log(data.msg);
                $('#mce-EMAIL').css('borderColor', '#ff8282');
                $('#subscribe-result').css('color', '#ff8282');
                $('#subscribe-result').html('<p>' + data.msg.substring(4) + '</p>');
            }
        }
    })
}

$('#play-video').on('click', function(e){
    e.preventDefault();
    $('#video-overlay').addClass('open');
    $("#video-overlay").append('<iframe width="560" height="315" src="https://www.youtube.com/embed/sBOhh9DW-7I?autoplay=1&controls=0" frameborder="0" allowfullscreen></iframe>');
});

$('.video-overlay, .video-overlay-close').on('click', function(e){
    e.preventDefault();
    close_video();
});

$(document).keyup(function(e){
    if(e.keyCode === 27) { close_video(); }
});

function close_video() {
    $('.video-overlay.open').removeClass('open').find('iframe').remove();
};
