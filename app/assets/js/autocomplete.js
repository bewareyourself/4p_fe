const countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];


function autocomplete(input, array) {
    var currentFocus;

    input.addEventListener("input", function (e) {
        var resultsContainer, resultItem, i, val = this.value;
        console.log(val);
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;

        resultsContainer = document.createElement("DIV");
        resultsContainer.setAttribute("id", this.id + "search-input__autocomplete-list");
        resultsContainer.setAttribute("class", "search-input__autocomplete-list-items");

        this.parentNode.appendChild(resultsContainer);

        for (i = 0; i < array.length; i++) {

            if (array[i].substr(0, val.length).toUpperCase() === val.toUpperCase()) {

                resultItem = document.createElement("DIV");

                resultItem.setAttribute("class", "search-input__autocomplete-list-item");

                resultItem.innerHTML = "<strong>" + array[i].substr(0, val.length) + "</strong>";
                resultItem.innerHTML += array[i].substr(val.length);

                resultItem.innerHTML += "<input type='hidden' value='" + array[i] + "'>";

                resultItem.addEventListener("click", function (e) {
                    input.value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                resultsContainer.appendChild(resultItem);
            }
        }
    });

    input.addEventListener("keydown", function (event) {
        let resultsList = document.getElementById(this.id + "search-input__autocomplete-list");

        if (resultsList) resultsList = resultsList.getElementsByTagName("div");

        if (event.keyCode === 40) {
            currentFocus++;
            addActive(resultsList);
        } else if (event.keyCode === 38) {
            currentFocus--;
            addActive(resultsList);
        } else if (event.keyCode === 13) {
            event.preventDefault();
            if (currentFocus > -1) {
                if (resultsList) resultsList[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        if (!x) return false;

        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);

        x[currentFocus].classList.add("active");
    }

    function removeActive(x) {

        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("active");
        }
    }

    function closeAllLists(element) {
        var x = document.getElementsByClassName("search-input__autocomplete-list-items");

        for (var i = 0; i < x.length; i++) {
            if (element !== x[i] && element !== input) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}



if (document.getElementById("autocompleteInput")) {
    autocomplete(document.getElementById("autocompleteInput"), countries);
}

if (document.getElementById("autocompleteCountry")) {
    autocomplete(document.getElementById("autocompleteCountry"), countries);
}

if (document.getElementById("autocompleteState")) {
    autocomplete(document.getElementById("autocompleteState"), countries);
}


//autocomplete(document.getElementById("autocompleteState"), countries);